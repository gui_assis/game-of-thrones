struct SeriesCastRequest: APIRequest {
    var method = RequestType.GET
    var path = "shows/%@/cast"

    init(seriesId: String) {
        path = String(format: path, seriesId)
    }
}
