import RxSwift

protocol SeriesServiceProtocol {
    func getSeriesDetail(seriesId: String) -> Observable<Series>
    func getSeriesCast(seriesId: String) -> Observable<[Persona]>
}

class SeriesService: SeriesServiceProtocol {
    let client: ClientProtocol

    init(client: ClientProtocol = BaseClient.sharedInstance) {
        self.client = client
    }

    func getSeriesDetail(seriesId: String) -> Observable<Series> {
        let request = SeriesDetailRequest(seriesId: seriesId)
        return client.requestSingle(request)
    }

    func getSeriesCast(seriesId: String) -> Observable<[Persona]> {
        let request = SeriesCastRequest(seriesId: seriesId)
        return client.requestList(request)
    }
}
