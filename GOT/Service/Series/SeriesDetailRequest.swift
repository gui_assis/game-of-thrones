struct SeriesDetailRequest: APIRequest {
    var method = RequestType.GET
    var path = "shows/%@"

    init(seriesId: String) {
        path = String(format: path, seriesId)
    }
}
