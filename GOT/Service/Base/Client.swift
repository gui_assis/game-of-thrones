import Foundation
import RxSwift

public protocol ClientProtocol {
    func requestList<Model: Decodable>(_ apiRequest: APIRequest) -> Observable<[Model]>
    func requestSingle<Model: Decodable>(_ apiRequest: APIRequest) -> Observable<Model>
}

public class BaseClient: ClientProtocol {

    private let baseURL = URL(string: "http://api.tvmaze.com/")!
    static let sharedInstance = BaseClient()

    open func requestList<Model: Decodable>(_ apiRequest: APIRequest) -> Observable<[Model]>{
        return Observable<[Model]>.create { [unowned self] observer in
            let request = apiRequest.request(with: self.baseURL)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    let model: [Model] = try JSONDecoder().decode([Model].self, from: data ?? Data())
                    observer.onNext(model)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
    }

    open func requestSingle<Model: Decodable>(_ apiRequest: APIRequest) -> Observable<Model> {
        return Observable<Model>.create { [unowned self] observer in
            let request = apiRequest.request(with: self.baseURL)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    let model: Model = try JSONDecoder().decode(Model.self, from: data ?? Data())
                    observer.onNext(model)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
    }
}
