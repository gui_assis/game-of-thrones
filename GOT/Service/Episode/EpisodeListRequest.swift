struct EpisodeListRequest: APIRequest {
    var method = RequestType.GET
    var path = "shows/%@/episodes"

    init(showId: String) {
        path = String(format: path, showId)
    }
}
