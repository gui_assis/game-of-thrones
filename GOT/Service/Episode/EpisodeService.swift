import RxSwift

protocol EpisodeServiceProtocol {
    func getEpisodeList() -> Observable<[Episode]>
}

class EpisodeService: EpisodeServiceProtocol {
    let client: ClientProtocol

    init(client: ClientProtocol = BaseClient.sharedInstance) {
        self.client = client
    }

    func getEpisodeList() -> Observable<[Episode]> {
        let request = EpisodeListRequest(showId: "82")
        return client.requestList(request)
    }
}
