import UIKit

struct ShowDetailRouter {
    static func pushShowDetailScreen(navigationController: UINavigationController?) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "showDetailViewController") as! ShowDetailViewController

        let presenter = ShowDetailPresenter.sharedInstance
        viewController.presenter = presenter

        navigationController?.pushViewController(viewController, animated: true)
    }
}
