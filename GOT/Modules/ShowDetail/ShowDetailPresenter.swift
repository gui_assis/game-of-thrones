import Foundation
import RxSwift

protocol ShowDetailPresenterProtocol {
    var showObservable: Observable<(Series, [Persona])> { get }
}

class ShowDetailPresenter: ShowDetailPresenterProtocol {

    static let sharedInstance = ShowDetailPresenter()

    let service: SeriesServiceProtocol
    let disposeBag = DisposeBag()

    var showObservable: Observable<(Series, [Persona])> {
        return Observable.zip(service.getSeriesDetail(seriesId: "82"), service.getSeriesCast(seriesId: "82"))
    }

    convenience init() {
        self.init(service: SeriesService())
    }

    init(service: SeriesServiceProtocol) {
        self.service = service
    }
}
