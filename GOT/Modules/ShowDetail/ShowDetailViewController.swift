import UIKit
import RxSwift
import RxCocoa

protocol ShowDetailViewProtocol {
    var presenter: ShowDetailPresenterProtocol? { get set }
}

class ShowDetailViewController: UIViewController, ShowDetailViewProtocol {

    @IBOutlet weak var collectionCharacters: UICollectionView!
    @IBOutlet weak var collectionCast: UICollectionView!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var imgShow: UIImageView!
    
    var presenter: ShowDetailPresenterProtocol?
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.showObservable
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: bindDetails)
            .disposed(by: disposeBag)

        collectionCast.delegate = nil
        collectionCast.dataSource = nil
        collectionCharacters.dataSource = nil
        collectionCharacters.delegate = nil
    }

    private func bindDetails(details: (Series, [Persona])) {
        imgShow.loadImageWithUrl(url: details
            .0.image.original, placeholder: nil)
        lblSummary.from(htmlString: details.0.summary)

        let items = Observable.just(details.1).share()
        items.asObservable()
            .bind(to: collectionCast.rx.items(cellIdentifier: "castCell", cellType: CastCell.self)) { row, persona, cell in
                cell.setupWithPerson(persona.person)
            }
            .disposed(by: disposeBag)

        items.asObservable()
            .bind(to: collectionCharacters.rx.items(cellIdentifier: "castCell", cellType: CastCell.self)) { row, persona, cell in
                cell.setupWithCharacter(persona.character)
            }
            .disposed(by: disposeBag)
    }
}
