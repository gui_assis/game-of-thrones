import UIKit
import RxSwift

protocol EpisodeDetailViewProtocol {
    var presenter: EpisodeDetailPresenterProtocol? { get set }
}

class EpisodeDetailViewController: UIViewController, EpisodeDetailViewProtocol {

    @IBOutlet weak var lblEpisodeName: UILabel!
    @IBOutlet weak var imgEpisode: UIImageView!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var btnShowDetail: UIButton!

    var presenter: EpisodeDetailPresenterProtocol?
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        btnShowDetail.layer.masksToBounds = true
        btnShowDetail.layer.cornerRadius = 5

        presenter?.episodeObservable
            .subscribe(onNext: bindEpisode)
            .disposed(by: disposeBag)

        presenter?.actionObservable
            .subscribe(onNext: handleAction)
            .disposed(by: disposeBag)
    }

    private func bindEpisode(episode: Episode) {
        lblEpisodeName.text = "\(episode.name) - E\(episode.number)S\(episode.season)"
        imgEpisode.loadImageWithUrl(url: episode.image.original, placeholder: nil)
        lblSummary.from(htmlString: episode.summary)
        title = episode.name
    }

    @IBAction func btnAddCalendarTap(_ sender: Any) {
        presenter?.addToCalendar()
    }

    @IBAction func btnShowDetailTap(_ sender: Any) {
        presenter?.showDetail()
    }

    private func handleAction(action: EpisodeDetailAction) {
        switch action {
        case .showMessage(let message):
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }
}
