import UIKit

class CastCell: UICollectionViewCell {
    @IBOutlet weak var imgCast: UIImageView!
    @IBOutlet weak var lblCastName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgCast.layer.masksToBounds = true
        imgCast.layer.cornerRadius = imgCast.frame.height / 2
    }

    func setupWithPerson(_ person: Person) {
        imgCast.loadImageWithUrl(url: person.image.medium, placeholder: UIImage(named: "placeholder"))
        lblCastName.text = person.name
    }

    func setupWithCharacter(_ character: Person) {
        imgCast.loadImageWithUrl(url: character.image.medium, placeholder: UIImage(named: "placeholder"))
        lblCastName.text = character.name
    }
}
