import Foundation
import RxSwift
import EventKit

enum EpisodeDetailAction {
    case showMessage(String)
}

protocol EpisodeDetailPresenterProtocol {
    var actionObservable: Observable<EpisodeDetailAction> { get }
    var episodeObservable: Observable<Episode> { get }
    var episode: Episode? { get set }
    var navigationController: UINavigationController? { get set }
    var eventStore: EventStoreProtocol { get set }

    func showDetail()
    func addToCalendar()
}

class EpisodeDetailPresenter: EpisodeDetailPresenterProtocol {

    static let sharedInstance = EpisodeDetailPresenter()

    let disposeBag = DisposeBag()
    var navigationController: UINavigationController?
    var eventStore: EventStoreProtocol
    var episode: Episode?

    private let actionSubject = PublishSubject<EpisodeDetailAction>()
    var actionObservable: Observable<EpisodeDetailAction> {
        return actionSubject.asObserver()
    }

    var episodeObservable: Observable<Episode> {
        guard let episode = episode else {
            return .empty()
        }

        return .just(episode)
    }

    convenience init() {
        self.init(eventStore: EventStore.sharedInstance)
    }

    init(eventStore: EventStoreProtocol) {
        self.eventStore = eventStore
    }

    func addToCalendar() {
        guard let episode = episode else {
            actionSubject.onNext(.showMessage("Failed to add event to calendar :("))
            return
        }

        eventStore.save(episode: episode) { result in
            switch result {
            case .fail(let message), .success(let message):
                self.actionSubject.onNext(.showMessage(message))
            }
        }
    }

    func showDetail() {
        ShowDetailRouter.pushShowDetailScreen(navigationController: navigationController)
    }
}

extension EpisodeDetailAction: Equatable {
    static func == (lhs: EpisodeDetailAction, rhs: EpisodeDetailAction) -> Bool {
        switch (lhs, rhs) {
        case let (.showMessage(a), .showMessage(b)):
            return a == b
        }
    }
}
