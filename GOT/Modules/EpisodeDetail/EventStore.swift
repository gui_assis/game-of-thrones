import UIKit
import Foundation
import EventKit

protocol EventStoreProtocol {
    func save(episode: Episode, completion: @escaping (EventStoreResult) -> Void)
}

enum EventStoreResult {
    case success(String)
    case fail(String)
}

struct EventStore: EventStoreProtocol {
    static let sharedInstance = EventStore()

    func save(episode: Episode, completion: @escaping (EventStoreResult) -> Void) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        guard let startDate = dateFormatter.date(from: episode.airdate) else {
            completion(.fail("Failed to add event to calendar :("))
            return
        }
        
        let endDate = Calendar.current.date(byAdding: .minute, value: Int(episode.runtime), to: startDate)
        let eventStore: EKEventStore = EKEventStore()

        eventStore.requestAccess(to: .event) { granted, error in

            if (granted) && (error == nil) {

                let event: EKEvent = EKEvent(eventStore: eventStore)

                event.title = "GOT - \(episode.name) - E\(episode.number)S\(episode.season)"
                event.startDate = startDate
                event.endDate = endDate
                event.notes = episode.summary
                event.calendar = eventStore.defaultCalendarForNewEvents

                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch {
                    completion(.fail("Failed to add event to calendar :("))
                }

                dateFormatter.dateFormat = "MM/dd/yyyy"
                completion(.success("Event added to date: \(dateFormatter.string(from: startDate))"))
            } else {
                completion(.fail("Failed to add event to calendar :("))
            }
        }
    }
}
