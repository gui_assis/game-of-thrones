import UIKit

struct EpisodeDetailRouter {
    static func pushEpisodeDetailScreen(episode: Episode, navigationController: UINavigationController?) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "episodeDetailViewController") as! EpisodeDetailViewController

        let presenter = EpisodeDetailPresenter.sharedInstance
        viewController.presenter = presenter
        presenter.episode = episode
        presenter.navigationController = navigationController

        navigationController?.pushViewController(viewController, animated: true)
    }
}
