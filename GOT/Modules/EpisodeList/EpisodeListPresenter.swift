import UIKit
import RxSwift

protocol EpisodeListPresenterProtocol {
    var episodeListObservable: Observable<[Episode]> { get }
    var navigationController: UINavigationController? { get set}

    func didSelectEpisode(episode: Episode)
}

public enum EpisodeListPresenterAction {
    case didSelectEpisode(Episode)
    case didLoadEpisodeList([Episode])
}

class EpisodeListPresenter: EpisodeListPresenterProtocol {

    var navigationController: UINavigationController?

    let service: EpisodeServiceProtocol
    let disposeBag = DisposeBag()

    var episodeListObservable: Observable<[Episode]> {
        return service.getEpisodeList()
    }

    convenience init() {
        self.init(service: EpisodeService())
    }

    init(service: EpisodeServiceProtocol) {
        self.service = service
    }

    func didSelectEpisode(episode: Episode) {
        EpisodeDetailRouter.pushEpisodeDetailScreen(episode: episode, navigationController: navigationController)
    }
}
