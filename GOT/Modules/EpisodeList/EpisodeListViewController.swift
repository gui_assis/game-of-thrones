import UIKit
import RxSwift
import RxCocoa

protocol EpisodeListViewProtocol {
    var presenter: EpisodeListPresenterProtocol? { get set }
    var episodeList: [Episode] { get set }
}

class EpisodeListViewController: UITableViewController, EpisodeListViewProtocol {

    var episodeList: [Episode] = []
    var presenter: EpisodeListPresenterProtocol?
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = nil
        tableView.dataSource = nil

        presenter?.episodeListObservable
            .bind(to: tableView.rx.items(cellIdentifier: "episodeCell", cellType: EpisodeCell.self)) { row, element, cell in
                cell.setupWithEpisode(element)
            }
            .disposed(by: disposeBag)

        tableView.rx.modelSelected(Episode.self)
            .subscribe(onNext: presenter?.didSelectEpisode)
            .disposed(by: disposeBag)
    }
}
