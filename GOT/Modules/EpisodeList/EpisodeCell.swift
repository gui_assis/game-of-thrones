import UIKit

class EpisodeCell: UITableViewCell {

    @IBOutlet weak var imgEpisode: UIImageView!
    @IBOutlet weak var lblEpisodeName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.2, 1.0]
        gradientLayer.frame = bounds
        imgEpisode.layer.insertSublayer(gradientLayer, at: 1)
    }

    func setupWithEpisode(_ episode: Episode) {
        imgEpisode.loadImageWithUrl(url: episode.image.original, placeholder: nil)
        lblEpisodeName.text = episode.name
    }
}
