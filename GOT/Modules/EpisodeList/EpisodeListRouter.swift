import UIKit

struct EpisodeListRouter {
    static func presentEpisodeListAsRoot(window: UIWindow) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "episodeListViewController") as! EpisodeListViewController
        let navigationController = UINavigationController(rootViewController: viewController)

        let presenter = EpisodeListPresenter()

        presenter.navigationController = navigationController
        viewController.presenter = presenter
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
