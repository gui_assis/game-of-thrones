import Foundation

public struct Image {
    let medium: String
    let original: String
}

extension Image: Decodable {
    enum ImageKeys: String, CodingKey {
        case medium
        case original
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ImageKeys.self)
        let medium: String = try container.decode(String.self, forKey: .medium)
        let original: String = try container.decode(String.self, forKey: .original)

        self.init(medium: medium, original: original)
    }
}

extension Image: Equatable {
    static public func ==(lhs: Image, rhs: Image) -> Bool {
        return lhs.medium == rhs.medium
            && lhs.original == rhs.original
    }
}
