import Foundation

struct Persona: Decodable {

    let person: Person
    let character: Person

    enum CodingKeys: String, CodingKey {
        case person = "person"
        case character = "character"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        person = try values.decode(Person.self, forKey: .person)
        character = try values.decode(Person.self, forKey: .character)
    }
}

extension Persona: Equatable {
    static func == (lhs: Persona, rhs: Persona) -> Bool {
        return lhs.person == rhs.person && lhs.character == rhs.character
    }
}
