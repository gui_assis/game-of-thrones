import Foundation

struct Series: Decodable {

	let name: String
	let image: Image
	let summary: String

	enum CodingKeys: String, CodingKey {
		case name
		case image
		case summary
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decode(String.self, forKey: .name)
		image = try values.decode(Image.self, forKey: .image)
		summary = try values.decode(String.self, forKey: .summary)
	}
}

extension Series: Equatable {
    static func == (lhs: Series, rhs: Series) -> Bool {
        return lhs.name == rhs.name
            && lhs.image == rhs.image
            && lhs.summary == rhs.summary
    }
}
