import Foundation

public struct Episode: Decodable {
    let name: String
    let season: Int
    let number: Int
    let summary: String
    let image: Image
    let runtime: Int
    let airdate: String

    enum EpisodeKeys: String, CodingKey {
        case name
        case season
        case number
        case summary
        case image
        case runtime
        case airdate
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: EpisodeKeys.self)
        name = try container.decode(String.self, forKey: .name)
        season = try container.decode(Int.self, forKey: .season)
        number = try container.decode(Int.self, forKey: .number)
        summary = try container.decode(String.self, forKey: .summary)
        image = try container.decode(Image.self, forKey: .image)
        runtime = try container.decode(Int.self, forKey: .runtime)
        airdate = try container.decode(String.self, forKey: .airdate)
    }
}

extension Episode: Equatable {
    static public func ==(lhs: Episode, rhs: Episode) -> Bool {
        return lhs.name == rhs.name
            && lhs.season == rhs.season
            && lhs.number == rhs.number
            && lhs.summary == rhs.summary
            && lhs.image == rhs.image
    }
}
