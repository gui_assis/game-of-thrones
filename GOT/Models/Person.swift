import Foundation

struct Person: Decodable {

    let name: String
    let image: Image

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        image = try values.decode(Image.self, forKey: .image)
    }
}

extension Person: Equatable {
    static func == (lhs: Person, rhs: Person) -> Bool {
        return lhs.name == rhs.name && lhs.image == rhs.image
    }
}
