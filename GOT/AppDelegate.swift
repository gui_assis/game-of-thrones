import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let disposeBag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        EpisodeListRouter.presentEpisodeListAsRoot(window: window!)
        applyAppearance()
        return true
    }
}

//MARK: - Application Appearence

extension AppDelegate {
    func applyAppearance() {
        UIApplication.shared.statusBarStyle = .lightContent

        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.font: UIFont(name: "OpenSans", size: 15)!
        ]

        UISearchBar.appearance().barTintColor = UIColor.gotBlackColor
        UISearchBar.appearance().tintColor = UIColor.white

        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.gotBlackColor
        UIBarButtonItem.appearance().tintColor = UIColor.white

        setupOpaqueNavigationBar()
    }

    func setupOpaqueNavigationBar() {
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.gotBlackColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barStyle = .black
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.font: UIFont(name: "OpenSans", size: 15)!
        ]
    }
}
