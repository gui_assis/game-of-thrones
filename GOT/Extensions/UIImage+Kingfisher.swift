import UIKit
import Kingfisher

public typealias ImageLoadCompletionBlock = (_ image: UIImage?) -> Void

class ImageHelper: NSObject {

    class func loadImageWithUrl(url: NSURL, completion: @escaping ImageLoadCompletionBlock) {
        guard let urlString = url.absoluteString, let url = URL(string: urlString) else {
            return
        }

        let downloader = KingfisherManager.shared.downloader
        let myCache = ImageCache(name: urlString)

        let optionInfo: KingfisherOptionsInfo = [
            .forceRefresh,
            .targetCache(myCache)
        ]

        downloader.downloadImage(with: url, options: optionInfo) { image, error, url, data in
            guard error == nil else {
                return
            }
            completion(image)
        }
    }
}

extension UIImageView {
    func loadImageWithUrl(url: String, placeholder: UIImage?) {
        if let _placeholder = placeholder {
            image = _placeholder
        }

        guard let url = NSURL(string: url) else {
            return
        }

        ImageHelper.loadImageWithUrl(url: url) { [weak self] image in
            self?.image = image
        }
    }
}
