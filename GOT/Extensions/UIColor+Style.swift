import UIKit

extension UIColor {
    class var gotBlackColor: UIColor {
        return UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)
    }
}
