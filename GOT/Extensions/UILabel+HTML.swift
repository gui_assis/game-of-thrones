import UIKit

extension UILabel {
    func from(htmlString: String) {
        let modifiedFont = String(format:"<span style=\"font-family: 'OpenSans'; font-size: \(font!.pointSize)\">%@</span>", htmlString)
        let htmlText = "<font color=\"#ffffff\">" + modifiedFont + "</font>"

        //process collection values
        let attrStr = try! NSAttributedString(
            data: htmlText.data(using: .unicode, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                      NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)

        attributedText = attrStr
    }
}
