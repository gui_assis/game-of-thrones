import Foundation
@testable import GOT

protocol DataProvider {
    var episodes: [Episode] { get }
    var show: Series { get }
}

extension DataProvider {
    var episodes: [Episode] {
        return try! getEpisodesFromFile()
    }

    fileprivate func getEpisodesFromFile() throws -> [Episode] {
        if let filePath = Bundle.main.path(forResource: "episodes", ofType: "json") {
            let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
            return try JSONDecoder().decode([Episode].self, from: data)
        }

        return []
    }

    var show: Series {
        return try! getShowFromFile()!
    }

    fileprivate func getShowFromFile() throws -> Series? {
        if let filePath = Bundle.main.path(forResource: "show", ofType: "json") {
            let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
            return try JSONDecoder().decode(Series.self, from: data)
        }

        return nil
    }

    var cast: [Persona] {
        return try! getCastFromFile()!
    }

    fileprivate func getCastFromFile() throws -> [Persona]? {
        if let filePath = Bundle.main.path(forResource: "cast", ofType: "json") {
            let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
            return try JSONDecoder().decode([Persona].self, from: data)
        }

        return nil
    }
}
