import Foundation
import RxBlocking
import XCTest

extension BlockingObservable {
    public func shouldThrowError() {
        do {
            _ = try self.first()
            XCTFail("Should've thrown error")
        } catch { }
    }
}

