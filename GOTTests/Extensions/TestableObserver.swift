import RxSwift
import RxTest
import XCTest

extension TestableObserver {
    public static func create() -> TestableObserver<Element> {
        let scheduler = TestScheduler(initialClock: 0, resolution: 0.1)
        return scheduler.createObserver(Element.self)
    }
}
