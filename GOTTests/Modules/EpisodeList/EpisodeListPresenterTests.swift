import XCTest
import RxSwift
import RxTest
import RxBlocking

@testable import GOT

class EpisodeListPresenterTests: XCTestCase, DataProvider {

    var presenter: EpisodeListPresenter!
    var mockEpisodeService: MockEpisodeService!

    override func setUp() {
        super.setUp()
        mockEpisodeService = MockEpisodeService()
        presenter = EpisodeListPresenter(service: mockEpisodeService)
    }

    func testEpisodeObservable() throws {
        mockEpisodeService.mockGetEpisodeList = Observable.just(self.episodes)

        guard let result = try presenter.episodeListObservable.toBlocking().first() else {
            return XCTFail("Did not get any result")
        }

        XCTAssertEqual(result, self.episodes)
        XCTAssertEqual(result.count, 3)
    }

    func testEpisodeObservableError() throws {
        mockEpisodeService.mockGetEpisodeList = Observable.error(AnyError())
        presenter.episodeListObservable.toBlocking().shouldThrowError()
    }
}

class MockEpisodeService: EpisodeServiceProtocol {
    var mockGetEpisodeList: Observable<[Episode]> = .empty()
    func getEpisodeList() -> Observable<[Episode]> {
        return mockGetEpisodeList
    }
}
