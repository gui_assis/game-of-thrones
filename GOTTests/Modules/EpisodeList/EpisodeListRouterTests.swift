import XCTest
@testable import GOT

class EpisodeListRouterTests: XCTestCase {

    var mockWindow: UIWindow!

    override func setUp() {
        super.setUp()
        mockWindow = UIWindow(frame: .zero)
    }

    func testModuleAssembly() {
        EpisodeListRouter.presentEpisodeListAsRoot(window: mockWindow)
        XCTAssertNotNil(mockWindow.rootViewController)
    }
}
