import XCTest
import RxBlocking
import RxTest
import RxSwift

@testable import GOT

class EpisodeDetailPresenterTests: XCTestCase, DataProvider {

    var presenter: EpisodeDetailPresenter!
    var mockEventStore: MockEventStore!
    let disposeBag = DisposeBag()

    override func setUp() {
        super.setUp()
        mockEventStore = MockEventStore()
        presenter = EpisodeDetailPresenter(eventStore: mockEventStore)
    }

    func testEpisodeDetailObservable() throws {
        presenter.episode = self.episodes.first

        guard let episode = try presenter.episodeObservable.toBlocking().first() else {
            return XCTFail("Did not get any result")
        }

        XCTAssertEqual(episode, self.episodes.first)
    }

    func testEpisodeDetailObservableEmpty() throws {
        XCTAssertNil(try presenter.episodeObservable.toBlocking().first())
    }

    func testAddEventToCalendarSuccess() throws {
        guard let episode = self.episodes.first else {
            return XCTFail("Did not get episode from data provider")
        }

        presenter.episode = episode
        presenter.addToCalendar()

        guard let result = try presenter.actionObservable.toBlocking().first() else {
            return XCTFail("Did not get any result")
        }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        guard let startDate = dateFormatter.date(from: episode.airdate) else {
            return XCTFail("Couldn't parse episode's date")
        }

        XCTAssertEqual(result, .showMessage("Event added to date: \(dateFormatter.string(from: startDate))"))
    }
}

class MockEventStore: EventStoreProtocol {
    var isSuccess: Bool = true

    func save(episode: Episode, completion: @escaping (EventStoreResult) -> Void) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        guard let startDate = dateFormatter.date(from: episode.airdate) else {
            return XCTFail("Couldn't parse episode's date")
        }

        isSuccess ? completion(.success("Event added to date: \(dateFormatter.string(from: startDate))")) : completion(.fail("Failed to add event to calendar :("))
    }
}
