import XCTest
@testable import GOT

class EpisodeDetailRouterTests: XCTestCase, DataProvider {

    override func setUp() {
        super.setUp()
    }

    func testModuleAssembly() {
        let mockNavigationController = UINavigationController(nibName: nil, bundle: nil)
        EpisodeDetailRouter.pushEpisodeDetailScreen(episode: self.episodes.first!, navigationController: mockNavigationController)

        XCTAssertEqual(mockNavigationController.viewControllers.count, 1)
    }
}
