import XCTest
import RxTest
import RxCocoa
import RxSwift

@testable import GOT

class ShowDetailPresenterTests: XCTestCase, DataProvider {

    var presenter: ShowDetailPresenter!
    var mockShowService: MockShowService!

    override func setUp() {
        super.setUp()
        mockShowService = MockShowService()
        presenter = ShowDetailPresenter(service: mockShowService)
    }

    func testShowObservable() throws {

        let expectedResult = (self.show, self.cast)

        mockShowService.mockGetSeriesDetail = Observable.just(self.show)
        mockShowService.mockGetSeriesCast = Observable.just(self.cast)

        guard let result = try presenter.showObservable.toBlocking().first() else {
            return XCTFail("Did not get any result")
        }

        XCTAssertEqual(result.0, expectedResult.0)
        XCTAssertEqual(result.1, expectedResult.1)
        XCTAssertEqual(result.1.count, 3)
    }
}


class MockShowService: SeriesServiceProtocol {

    var mockGetSeriesDetail: Observable<Series> = .empty()
    func getSeriesDetail(seriesId: String) -> Observable<Series> {
        return mockGetSeriesDetail
    }

    var mockGetSeriesCast: Observable<[Persona]> = .empty()
    func getSeriesCast(seriesId: String) -> Observable<[Persona]> {
        return mockGetSeriesCast
    }
}
