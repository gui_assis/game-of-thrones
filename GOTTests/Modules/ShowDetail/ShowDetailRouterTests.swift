import XCTest
@testable import GOT

class ShowDetailRouterTests: XCTestCase, DataProvider {

    override func setUp() {
        super.setUp()
    }

    func testModuleAssembly() {
        let mockNavigationController = UINavigationController(nibName: nil, bundle: nil)
        ShowDetailRouter.pushShowDetailScreen(navigationController: mockNavigationController)

        XCTAssertEqual(mockNavigationController.viewControllers.count, 1)
    }
}
